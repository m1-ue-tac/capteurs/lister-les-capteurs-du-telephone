package com.univlille.listedescapteurs;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

/**
 * Exemple d'une application qui liste les capteurs de votre téléphone (liste non exhaustive)
 */
public class MainActivity extends AppCompatActivity {

    // Le gestionnaire de capteurs
    SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // On récupère le gestionnaire de capteurs
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // Liste des capteurs du téléphone
        listSensors();
    }

    // Listing des capteurs du téléphone
    private void listSensors() {
        // On récupère tous les capteurs du téléphone (de tout type)
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);

        StringBuffer description_des_capteurs = new StringBuffer();
        // pour chaque capteur, on ajoute sa description au résultat à afficher
        for (Sensor sensor : sensors) {
            description_des_capteurs.append("Capteur_____ : \r\n");
            description_des_capteurs.append("\tNom : " + sensor.getName() + "\r\n");
            description_des_capteurs.append("\tType : " + getType(sensor.getType()) + "\r\n");
            description_des_capteurs.append("Version : " + sensor.getVersion() + "\r\n");
            description_des_capteurs.append("Résolution : " + sensor.getResolution() + "\r\n");
            description_des_capteurs.append("Puissance en mA : " + sensor.getPower() + "\r\n");
            description_des_capteurs.append("Constructeur : " + sensor.getVendor() + "\r\n");
            description_des_capteurs.append("Valeur Max :" + sensor.getMaximumRange() + "\r\n");
            description_des_capteurs.append("Valeur Min : " + sensor.getMinDelay() + "\r\n");
        }
        Log.d("résultat", description_des_capteurs.toString());

        // Pour trouver un capteur en particulier, par exemple le gyroscope ici, on écrit :
        Sensor gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        // Pour trouver tous les gyroscopes, on écrira :
        List<Sensor> gyroscopes = sensorManager.getSensorList(Sensor.TYPE_GYROSCOPE);
    }

    private String getType(int type) {
        String strType;
        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
                strType = "TYPE_ACCELEROMETER";
                break;
            case Sensor.TYPE_GRAVITY:
                strType = "TYPE_GRAVITY";
                break;
            case Sensor.TYPE_GYROSCOPE:
                strType = "TYPE_GYROSCOPE";
                break;
            case Sensor.TYPE_LIGHT:
                strType = "TYPE_LIGHT";
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                strType = "TYPE_LINEAR_ACCELERATION";
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                strType = "TYPE_MAGNETIC_FIELD";
                break;
            case Sensor.TYPE_PRESSURE:
                strType = "TYPE_PRESSURE";
                break;
            case Sensor.TYPE_PROXIMITY:
                strType = "TYPE_PROXIMITY";
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                strType = "TYPE_ROTATION_VECTOR";
                break;
            case Sensor.TYPE_STEP_COUNTER:
                strType = "TYPE_STEP_COUNTER";
                break;
            default:
                strType = "TYPE_UNKNOW";
                break;
        }
        return strType;
    }
}